<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/template.css') }}">
        <title> {{ config('app.name') }} </title>
    </head>
    <body>
        @include('inc/navbar')
        <div class="container-fluid">
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                @include('inc/sidenav')
                @include('inc/message')
                <div class="mt-5"> @yield('content') </div>
            </main>
        </div>
        @include('inc/footer')
        @include('inc/scripts')
    </body>
</html>
