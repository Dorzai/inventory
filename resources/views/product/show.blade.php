@extends('layout/app')
@section('content')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4"> {{ $product->name }} </h1>
        <p class="lead"> {{ $product->name }} a populair now being sold for ${{ $product->price }} on the internet. </p>
        <a href="/product" class="mt-4 btn btn-primary"> Return </a>
    </div>
@endsection
