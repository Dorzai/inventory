@extends('layout/app')
@section('content')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Products </h1>
        <p class="lead"> All the games that we sell have been assigned in this table. These are only used for demonstrational purposes. We are not selling games without license, this is a project to understand how MVC models work </p>
    </div>

    @if(count($products) > 0)
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Game</th>
                    <th scope="col">Price</th>
                    <th scope="col">Status</th>
                </tr>
            </thead>
            @foreach($products as $product)
                <tbody>
                    <tr>
                        <td> <a href="/product/{{ $product->id }}"> {{ $product->name }} </a> </td>
                        <td> ${{ $product->price }} </td>

                        @if($product->sold > 0)
                            <td> Sold </td>
                        @else
                            <td> On sale </td>
                        @endif
                    </tr>
                </tbody>
            @endforeach
        </table>
    @endif
@endsection
