@extends('layout/app')
@section('content')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4"> Create </h1>
        {!! Form::open(['action' => 'ProductController@store', 'method' => 'POST']) !!}
            <div class="form-group">
                {{ Form::label('name', 'Product name') }}
                {{ Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Minecraft']) }}
            </div>
            {{ Form::select('warehouses', $warehouse) }}
        {!! Form::close() !!}
        <a href="/product" class="mt-4 btn btn-primary"> Return </a>
    </div>
@endsection
