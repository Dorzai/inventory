<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Products</span>
            <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="plus-circle"></span>
            </a>
        </h6>
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="/">
                    <span data-feather="shopping-cart"></span>
                    Full data listing
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/product/create">
                    Add new product
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">
                    View sold products
                </a>
            </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Warehouses</span>
            <a class="d-flex align-items-center text-muted" href="#">
                <span data-feather="plus-circle"></span>
            </a>
        </h6>
        <ul class="nav flex-column mb-2">
            <li class="nav-item">
                <a class="nav-link" href="/warehouse">
                    Full data listing
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/warehouse/create">
                    Add warehouse
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">
                    Display full warehouses
                </a>
            </li>
        </ul>
    </div>
</nav>
