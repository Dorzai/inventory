@extends('layout/app')
@section('content')
    @if(count($warehouses) > 0)
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Status</th>
                    <th scope="col">City</th>
                    <th scope="col">Cost</th>
                    <td scope="col">Edit</td>
                    <td scope="col">Delete</td>
                </tr>
            </thead>
            @foreach($warehouses as $warehouse)
                <tbody>
                    <tr>
                        <td scope="row"> <a href="/warehouse/{{ $warehouse->id }}"> {{ $warehouse->name }} </a> </td>
                        <td scope="row"> {{ $warehouse->current_capacity }}/{{ $warehouse->max_capacity }} </td>
                        <td scope="row"> {{ $warehouse->find($warehouse->id)->location->city }} </td>
                        <td scope="row"> ${{ $warehouse->rent }} </td>
                        <td scope="row"> <a href="/warehouse/{{ $warehouse->id }}/edit"> Edit </a> </td>
                        <td scope="row"> <a href="/warehouse/{{ $warehouse->id }}/warning"> Delete </a> </td>
                    </tr>
                </tbody>
            @endforeach

        </table>
    @endif
@endsection
