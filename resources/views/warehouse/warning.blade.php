@extends('layout/app')
@section('content')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4"> {{ $warehouse->name }} </h1>
        <p class="lead"> Are you sure that you want to delete this? If you press okay you won't be able to recover any lost data. </p>
        {!! Form::open(['action' => ['WarehouseController@destroy', $warehouse->id], 'method' => 'POST']) !!}
            {{ Form::hidden('_method', 'DELETE') }}
            {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-lg']) }}
        {!! Form::close() !!}
    </div>
@endsection
