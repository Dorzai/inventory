@extends('layout/app')
@section('content')
    <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Status</th>
            <th scope="col">Rent</th>
            <th scope="col">Streetname</th>
            <th scope="col">Number</th>
            <th scope="col">Zip code</th>
            <th scope="col">City</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
            <tbody>
            <tr>
                <td scope="row"> {{ $warehouse->name }} </td>
                <td scope="row"> {{ $warehouse->current_capacity }}/{{ $warehouse->max_capacity }} </td>
                <td scope="row"> ${{ $warehouse->rent }} </td>
                <td scope="row"> {{ $warehouse->location->street }} </td>
                <td scope="row"> {{ $warehouse->location->number }} </td>
                <td scope="row"> {{ $warehouse->location->zip }} </td>
                <td scope="row"> {{ $warehouse->location->city }} </td>
                <td scope="row"> <a href="/warehouse/{{ $warehouse->id }}/edit"> Edit </a> </td>
                <td scope="row"> <a href="/warehouse/{{ $warehouse->id }}/warning"> Delete </a> </td>
            </tr>
        </tbody>
    </table>
@endsection
