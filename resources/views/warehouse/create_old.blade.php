@extends('layout/app')
@section('content')
    <div class="ml-5 w-100 d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        {!! Form::open(['action' => 'WarehouseController@store', 'method' => 'POST', 'class' => 'w-100']) !!}
            <div class="col-md-13 mb-3 text-left">
                {{ Form::label('name', 'Warehouse name') }}
                {{ Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Boston Warehouse Facility']) }}
            </div>
            <div class="row text-left">
                <div class="col-md-6 mb-3">
                    {{ Form::label('max_capacity', 'Max capacity') }}
                    {{ Form::number('max_capacity', '', ['class' => 'form-control', 'placeholder' => '300']) }}
                </div>
                <div class="col-md-6 mb-3">
                    {{ Form::label('rent', 'Rent') }}
                    {{ Form::number('rent', '', ['class' => 'form-control', 'placeholder' => '3995,56']) }}
                </div>
            </div>
            <div class="row text-left">
                <div class="col-md-9 mb-3">
                    {{ Form::label('street', 'Streetname') }}
                    {{ Form::text('street', '', ['class' => 'form-control', 'placeholder' => 'Filatalia Rd']) }}
                </div>
                <div class="col-md-3 mb-3">
                    {{ Form::label('number', 'Number') }}
                    {{ Form::text('number', '', ['class' => 'form-control', 'placeholder' => 'C344']) }}
                </div>
            </div>
            <div class="row text-left mb-3">
                <div class="col-md-6 mb-3">
                    {{ Form::label('zip', 'Zip-code') }}
                    {{ Form::text('zip', '', ['class' => 'form-control', 'placeholder' => '5226YU']) }}
                </div>
                <div class="col-md-6 mb-3">
                    {{ Form::label('city', 'City') }}
                    {{ Form::text('city', '', ['class' => 'form-control', 'placeholder' => 'New York']) }}
                </div>
            </div>
            {{ Form::submit('Submit', ['class'=>'btn btn-success w-100']) }}
        {!! Form::close() !!}
    </div>
@endsection
