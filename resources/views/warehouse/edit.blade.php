@extends('layout/app')
@section('content')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        {!! Form::open(['action' => ['WarehouseController@update', $warehouse->id], 'method' => 'POST']) !!}
        <div class="col-md-13 mb-3 text-left">
            {{ Form::label('name', 'Warehouse name') }}
            {{ Form::text('name', $warehouse->name, ['class' => 'form-control', 'placeholder' => 'Boston Warehouse Facility']) }}
        </div>
        <div class="row text-left">
            <div class="col-md-6 mb-3">
                {{ Form::label('max_capacity', 'Max capacity') }}
                {{ Form::number('max_capacity', $warehouse->max_capacity, ['class' => 'form-control', 'placeholder' => '300']) }}
            </div>
            <div class="col-md-6 mb-3">
                {{ Form::label('rent', 'Rent') }}
                {{ Form::number('rent', $warehouse->rent, ['class' => 'form-control', 'placeholder' => '3995,56']) }}
            </div>
        </div>
        <div class="row text-left">
            <div class="col-md-9 mb-3">
                {{ Form::label('street', 'Streetname') }}
                {{ Form::text('street', $warehouse->location->street, ['class' => 'form-control', 'placeholder' => 'Filatalia Rd']) }}
            </div>
            <div class="col-md-3 mb-3">
                {{ Form::label('number', 'Number') }}
                {{ Form::text('number', $warehouse->location->number, ['class' => 'form-control', 'placeholder' => 'C344']) }}
            </div>
        </div>
        <div class="row text-left mb-3">
            <div class="col-md-6 mb-3">
                {{ Form::label('zip', 'Zip-code') }}
                {{ Form::text('zip', $warehouse->location->zip, ['class' => 'form-control', 'placeholder' => '5226YU']) }}
            </div>
            <div class="col-md-6 mb-3">
                {{ Form::label('city', 'City') }}
                {{ Form::text('city', $warehouse->location->city, ['class' => 'form-control', 'placeholder' => 'New York']) }}
            </div>
        </div>
        {{ Form::hidden('_method', 'PUT') }}
        {{ Form::submit('Submit', ['class'=>'btn btn-success w-100']) }}
        {!! Form::close() !!}
    </div>
@endsection
