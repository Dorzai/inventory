@extends('layout/app')
@section('content')
<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Company name</a>
    <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
    <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
            <a class="nav-link" href="#">Sign out</a>
        </li>
    </ul>
</nav>
    {!! Form::open(['action' => 'WarehouseController@store', 'method' => 'POST', 'class' => 'w-100']) !!}
    <div class="col-md-13 mb-3 text-left">
        {{ Form::label('name', 'Warehouse name') }}
        {{ Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Boston Warehouse Facility']) }}
    </div>
    <div class="row text-left">
        <div class="col-md-6 mb-3">
            {{ Form::label('max_capacity', 'Max capacity') }}
            {{ Form::number('max_capacity', '', ['class' => 'form-control', 'placeholder' => '300']) }}
        </div>
        <div class="col-md-6 mb-3">
            {{ Form::label('rent', 'Rent') }}
            {{ Form::number('rent', '', ['class' => 'form-control', 'placeholder' => '3995,56']) }}
        </div>
    </div>
    <div class="row text-left">
        <div class="col-md-9 mb-3">
            {{ Form::label('street', 'Streetname') }}
            {{ Form::text('street', '', ['class' => 'form-control', 'placeholder' => 'Filatalia Rd']) }}
        </div>
        <div class="col-md-3 mb-3">
            {{ Form::label('number', 'Number') }}
            {{ Form::text('number', '', ['class' => 'form-control', 'placeholder' => 'C344']) }}
        </div>
    </div>
    <div class="row text-left mb-3">
        <div class="col-md-6 mb-3">
            {{ Form::label('zip', 'Zip-code') }}
            {{ Form::text('zip', '', ['class' => 'form-control', 'placeholder' => '5226YU']) }}
        </div>
        <div class="col-md-6 mb-3">
            {{ Form::label('city', 'City') }}
            {{ Form::text('city', '', ['class' => 'form-control', 'placeholder' => 'New York']) }}
        </div>
    </div>
    {{ Form::submit('Submit', ['class'=>'btn btn-success w-100']) }}
    {!! Form::close() !!}
@endsection
