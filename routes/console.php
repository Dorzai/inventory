<?php

use Illuminate\Foundation\Inspiring;

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('add:product {name} {price}', function ($name, $price) {
    $product = ['name' => $name, 'price' => $price, 'sold' => false];
    \App\Models\Base\Product::create($product);
    $this->info($name . ' has been inplemented into the database.');
});

Artisan::command('add:warehouse', function () {
});
