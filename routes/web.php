<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::get('/', 'ProductController@index');

route::resource('/product', 'ProductController');

route::get('/warehouse/{warehouse}/warning', 'WarehouseController@warning');
route::resource('/warehouse', 'WarehouseController');
