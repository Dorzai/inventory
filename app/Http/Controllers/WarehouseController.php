<?php

namespace App\Http\Controllers;

use App\Models\Base\Warehouse;
use App\Models\Base\Location;
use Illuminate\Http\Request;

class WarehouseController extends Controller {
    public function index() {
        // The home page for the warehouses
        $warehouses = Warehouse::with('location')->get();
        $list = Warehouse::pluck('name', 'location_id');
        return view('warehouse/index')->with(['warehouses' => $warehouses, 'list' => $list]);
    }

    public function create() {
        // Return to the creation page for the warehouses
        return view('warehouse/create');
    }

    public function store(Request $request) {
        // Create a warehouse data entry

        // Create new table entities
        $warehouse = new Warehouse;
        $location  = new Location;

        // Create a new entity in the database
        $this->InputData($request, $warehouse, $location, true);

        // After storing data from warehouse return to the warehouse homepage
        return redirect('/warehouse')->with('success', 'Warehouse has been added!');
    }

    public function show(Warehouse $warehouse) {
        // Show the specific warehouse data
        return view('warehouse/show')->with('warehouse', $warehouse);
    }

    public function edit(Warehouse $warehouse) {
        // Go to the warehouse edit page
        return view('warehouse/edit')->with('warehouse', $warehouse);
    }

    public function update(Request $request, Warehouse $warehouse) {
        $warehouse = Warehouse::find($warehouse->id);         // Get the correct warehouse entity
        $location  = Location::find($warehouse->location_id); // Get the correct location entity

        // Update old data
        $this->InputData($request, $warehouse, $location, false);

        // Return back to warehouse home page
        return redirect('/warehouse')->with('success', 'Warehouse has been updated!');
    }

    public function warning(Warehouse $warehouse) {
        return view('/warehouse/warning')->with('warehouse', $warehouse);
    }

    public function destroy(Warehouse $warehouse) {
        $warehouse = Warehouse::find($warehouse->id);
        $save = $warehouse->name;

        $warehouse->delete();

        return redirect('/warehouse')->with('success', $save . ' has been removed.');
    }

    // Self created functions
    public function InputData(Request $request, Warehouse $warehouse, Location $location, $new) {
        // Make sure all fields are filled in
        $this->validate($request, ['name' => 'required', 'max_capacity' => 'required', 'rent' => 'required', 'street' => 'required',
                                   'number' => 'required', 'zip' => 'required', 'city' => 'required']);

        // Get all the location data and enter it into the database
        $location->street = $request->input('street');
        $location->number = $request->input('number');
        $location->zip = $request->input('zip');
        $location->city = $request->input('city');

        // Save data to database
        $location->save();

        // Get all the warehouse data and enter it into the database
        $warehouse->name = $request->input('name');
        if ($new) {
            $warehouse->current_capacity = 0;
            $warehouse->location_id = $location->id;
        }
        $warehouse->max_capacity = $request->input('max_capacity');
        $warehouse->rent = $request->input('rent');

        // Save data to database
        $warehouse->save();
    }
}
