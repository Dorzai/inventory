<?php

namespace App\Http\Controllers;

use App\Models\Base\Product;
use App\Models\Base\Warehouse;
use Illuminate\Http\Request;

class ProductController extends Controller {
    public function index() {
        $products = Product::all();
        return view('product/index')->with('products', $products);
    }

    public function create() {
        $warehouse = Warehouse::whereRaw('current_capacity < max_capacity')->pluck('name', 'id');
        $product = Product::all();

        return view('product/create')->with(['product' => $product, 'warehouse' => $warehouse]);
    }

    public function store(Request $request) {
        echo 'hello world';
    }

    public function show(Product $product) {
        return view('product/show')->with('product', $product);
    }

    public function edit($id) {
        //
    }

    public function update(Request $request, $id) {
        //
    }

    public function destroy($id) {
        //
    }
}
